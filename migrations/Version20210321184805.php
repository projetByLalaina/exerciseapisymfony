<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210321184805 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE kraken (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(50) NOT NULL, age INT NOT NULL, taille VARCHAR(10) NOT NULL, poids INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pouvoir (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tentacules (id INT AUTO_INCREMENT NOT NULL, kraken_id INT NOT NULL, pouvoir_id INT NOT NULL, nom VARCHAR(50) NOT NULL, INDEX IDX_60AC3228A9341DD (kraken_id), INDEX IDX_60AC322C8A705F8 (pouvoir_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tentacules ADD CONSTRAINT FK_60AC3228A9341DD FOREIGN KEY (kraken_id) REFERENCES kraken (id)');
        $this->addSql('ALTER TABLE tentacules ADD CONSTRAINT FK_60AC322C8A705F8 FOREIGN KEY (pouvoir_id) REFERENCES pouvoir (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tentacules DROP FOREIGN KEY FK_60AC3228A9341DD');
        $this->addSql('ALTER TABLE tentacules DROP FOREIGN KEY FK_60AC322C8A705F8');
        $this->addSql('DROP TABLE kraken');
        $this->addSql('DROP TABLE pouvoir');
        $this->addSql('DROP TABLE tentacules');
    }
}
