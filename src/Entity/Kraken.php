<?php

namespace App\Entity;

use App\Repository\KrakenRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=KrakenRepository::class)
 */
class Kraken
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nom;

    /**
     * @ORM\Column(type="integer")
     */
    private $age;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $taille;

    /**
     * @ORM\Column(type="integer")
     */
    private $poids;

    /**
     * @ORM\OneToMany(targetEntity=Tentacules::class, mappedBy="kraken")
     */
    private $tentacules;

    public function __construct()
    {
        $this->tentacules = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getAge(): ?int
    {
        return $this->age;
    }

    public function setAge(int $age): self
    {
        $this->age = $age;

        return $this;
    }

    public function getTaille(): ?string
    {
        return $this->taille;
    }

    public function setTaille(string $taille): self
    {
        $this->taille = $taille;

        return $this;
    }

    public function getPoids(): ?int
    {
        return $this->poids;
    }

    public function setPoids(int $poids): self
    {
        $this->poids = $poids;

        return $this;
    }

    /**
     * @return Collection|Tentacules[]
     */
    public function getTentacules(): Collection
    {
        return $this->tentacules;
    }

    public function addTentacule(Tentacules $tentacule): self
    {
        if (!$this->tentacules->contains($tentacule)) {
            $this->tentacules[] = $tentacule;
            $tentacule->setKraken($this);
        }

        return $this;
    }

    public function removeTentacule(Tentacules $tentacule): self
    {
        if ($this->tentacules->removeElement($tentacule)) {
            // set the owning side to null (unless already changed)
            if ($tentacule->getKraken() === $this) {
                $tentacule->setKraken(null);
            }
        }

        return $this;
    }
}
