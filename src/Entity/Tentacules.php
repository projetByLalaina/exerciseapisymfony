<?php

namespace App\Entity;

use App\Repository\TentaculesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TentaculesRepository::class)
 */
class Tentacules
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nom;

    /**
     * @ORM\ManyToOne(targetEntity=Kraken::class, inversedBy="tentacules")
     * @ORM\JoinColumn(nullable=false)
     */
    private $kraken;

    /**
     * @ORM\ManyToOne(targetEntity=Pouvoir::class, inversedBy="tentacules")
     * @ORM\JoinColumn(nullable=false)
     */
    private $pouvoir;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getKraken(): ?Kraken
    {
        return $this->kraken;
    }

    public function setKraken(?Kraken $kraken): self
    {
        $this->kraken = $kraken;

        return $this;
    }

    public function getPouvoir(): ?Pouvoir
    {
        return $this->pouvoir;
    }

    public function setPouvoir(?Pouvoir $pouvoir): self
    {
        $this->pouvoir = $pouvoir;

        return $this;
    }
}
