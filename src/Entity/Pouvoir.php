<?php

namespace App\Entity;

use App\Repository\PouvoirRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PouvoirRepository::class)
 */
class Pouvoir
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\OneToMany(targetEntity=Tentacules::class, mappedBy="pouvoir")
     */
    private $tentacules;

    public function __construct()
    {
        $this->tentacules = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection|Tentacules[]
     */
    public function getTentacules(): Collection
    {
        return $this->tentacules;
    }

    public function addTentacule(Tentacules $tentacule): self
    {
        if (!$this->tentacules->contains($tentacule)) {
            $this->tentacules[] = $tentacule;
            $tentacule->setPouvoir($this);
        }

        return $this;
    }

    public function removeTentacule(Tentacules $tentacule): self
    {
        if ($this->tentacules->removeElement($tentacule)) {
            // set the owning side to null (unless already changed)
            if ($tentacule->getPouvoir() === $this) {
                $tentacule->setPouvoir(null);
            }
        }

        return $this;
    }
}
